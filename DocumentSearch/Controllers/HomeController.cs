﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DocumentSearch.Api;
using DocumentSearch.Api.Configuration;
using DocumentSearch.Api.DataTransferObjects;

namespace DocumentSearch.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index(string queryInput = "")
        {
            var config = DocumentSearchConfiguration.Current();

            var request = new SearchRequest()
            {
                ResultType = TypeOfResult.Documents,
                Start = 0,
                RowsPerPage = 25,
                Queries = new List<SearchRequestQuery>(),
                Filters = new List<Facet>(),
            };
            request.Queries.Add(new SearchRequestQuery()
            {
                Name = "Document Search",
                Query = queryInput,
            });

            SearchManager search = new SearchManager(config.SearchConfiguration);
            search.HandleRequest(request);
            search.ExecuteAllQueries();

            return View(search.Response);
        }

    }
}
