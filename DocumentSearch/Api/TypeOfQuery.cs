﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocumentSearch.Api
{
    public enum TypeOfQuery
    {
        Company,
        CompanyAggregation,
        CompanyResult,
        Document,
        Overlap,
        Project,
        Aggregation,
        Universe
    }
}