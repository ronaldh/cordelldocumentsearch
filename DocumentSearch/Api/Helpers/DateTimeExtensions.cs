﻿using System;

namespace DocumentSearch.Api.Helpers
{
    public static class DateTimeExtensions
    {
        public static int MonthsSince20000101(this DateTime value)
        {
            var yearDifference = value.Year - 2000;
            return yearDifference * 12 + value.Month - 1;
        }
    }
}