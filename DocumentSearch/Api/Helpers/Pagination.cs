﻿using System;

namespace DocumentSearch.Api.Helpers
{
    /// <summary>
    /// Represents pagination details.
    /// </summary>
    public class Pagination
    {
        #region Enumeration

        public enum PaginationGroupDisplayType { Standard, Rolling };

        #endregion

        #region Private Member Variables

        private PaginationGroupDisplayType _groupDisplayType = PaginationGroupDisplayType.Standard;

        private long _maxOffset;

        private long _recordsPerPage;
        private long _currentOffset;
        private long _totalRecords;

        private long _recordsAvailableOnPage;
        private long _pageCount;
        private long _currentPageNo;
        private long _nextPageNo;
        private long _previousPageNo;
        private long _nextOffset;
        private long _previousOffset;
        private long _pagesPerGroup;
        private long _previousGroupLastPageNo;
        private long _nextGroupFirstPageNo;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="recordsPerPage">Number of records displayed per page.</param>
        /// <param name="currentOffset">Current number of records offset.</param>
        /// <param name="totalRecords">Total number of records available.</param>
        public Pagination(long recordsPerPage, long currentOffset, long totalRecords, long maxOffset)
        {
            _maxOffset = maxOffset;
            _recordsPerPage = recordsPerPage;
            _currentOffset = currentOffset;
            _totalRecords = totalRecords;
        }

        /// <summary>
        /// Setup page groups.
        /// </summary>
        /// <param name="recordsPerPage">Number of records displayed per page.</param>
        /// <param name="currentOffset">Current number of records offset.</param>
        /// <param name="totalRecords">Total number of records available.</param>
        /// <param name="pagesPerGroup">Display page numbers displayed as a set.</param>
        public Pagination(long recordsPerPage, long currentOffset, long totalRecords, long pagesPerGroup, long maxOffset)
            : this(recordsPerPage, currentOffset, totalRecords, maxOffset)
        {
            _pagesPerGroup = pagesPerGroup;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Calculate pagination details.
        /// </summary>
        private void CalculateInternal()
        {
            _pageCount = (int)Math.Ceiling((double)_totalRecords / (double)_recordsPerPage);

            _currentPageNo = (int)Math.Ceiling((double)_currentOffset / (double)_recordsPerPage) + 1;
            _recordsAvailableOnPage = Math.Min(_recordsPerPage, _totalRecords - _currentOffset);

            _nextPageNo = _currentPageNo + 1;
            _nextOffset = _currentOffset + _recordsPerPage;

            _previousPageNo = _currentPageNo - 1;
            _previousOffset = _currentOffset - _recordsPerPage;

            if (_previousOffset < 0)
            {
                _previousOffset = 0;
            }

            if (_pagesPerGroup != 0)
            {
                switch (_groupDisplayType)
                {
                    case PaginationGroupDisplayType.Standard:
                        _nextGroupFirstPageNo = (int)Math.Ceiling((double)_currentPageNo / (double)_pagesPerGroup) * _pagesPerGroup + 1;

                        if (_nextGroupFirstPageNo > _pageCount)
                        {
                            _nextGroupFirstPageNo = 0;
                        }

                        _previousGroupLastPageNo = (int)Math.Truncate((double)_currentPageNo / (double)_pagesPerGroup) * _pagesPerGroup;

                        if (_previousGroupLastPageNo == _currentPageNo)
                        {
                            _previousGroupLastPageNo -= _pagesPerGroup;
                        }
                        break;
                    case PaginationGroupDisplayType.Rolling:
                        _nextGroupFirstPageNo = _currentPageNo + (int)Math.Truncate((double)_pagesPerGroup / 2) + 1;

                        if (_nextGroupFirstPageNo < _pagesPerGroup + 1)
                        {
                            _nextGroupFirstPageNo = _pagesPerGroup + 1;
                        }

                        if (_nextGroupFirstPageNo > _pageCount)
                        {
                            _nextGroupFirstPageNo = 0;
                        }

                        _previousGroupLastPageNo = _currentPageNo - (int)Math.Ceiling((double)_pagesPerGroup / 2);

                        if (_previousGroupLastPageNo > _pageCount - _pagesPerGroup)
                        {
                            _previousGroupLastPageNo = _pageCount - _pagesPerGroup;
                        }

                        if (_previousGroupLastPageNo < 0)
                        {
                            _previousGroupLastPageNo = 0;
                        }

                        break;
                }
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Calculate pagination details.
        /// </summary>
        public void Calculate()
        {
            CalculateInternal();
        }

        /// <summary>
        /// Calculate pagination details based on input.
        /// </summary>
        /// <param name="recordsPerPage">Number of records displayed per page.</param>
        /// <param name="currentOffset">Current number of records offset.</param>
        /// <param name="totalRecords">Total number of records available.</param>
        public void Calculate(long recordsPerPage, long currentOffset, long totalRecords)
        {
            _recordsPerPage = recordsPerPage;
            _currentOffset = currentOffset;
            _totalRecords = totalRecords;

            CalculateInternal();
        }

        /// <summary>
        /// Get the offset for the page number provided.
        /// </summary>
        /// <param name="pageNo">Page number.</param>
        /// <returns>Offset for the page number.</returns>
        public long GetOffset(long pageNo)
        {
            return (pageNo - 1) * _recordsPerPage;
        }


        /// <summary>
        /// Checks the next group's offset validity
        /// </summary>
        /// <returns>true when max offset has not been hit</returns>
        public bool IsValidNextGroup()
        {
            if (NextGroupFirstPageNo < ((_maxOffset / RecordsPerPage) + 1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Checks the pageNo's offset validity
        /// </summary>
        /// <param name="pageNo">Page number</param>
        /// <returns>true when the page's offset is valid</returns>
        public bool IsValidPageOffset(long pageNo)
        {
            if (pageNo <= ((_maxOffset / RecordsPerPage) + 1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Get or set the group display type.
        /// </summary>
        public PaginationGroupDisplayType GroupDisplayType
        {
            get
            {
                return _groupDisplayType;
            }

            set
            {
                _groupDisplayType = value;
            }
        }

        /// <summary>
        /// Returns constant value for max offset
        /// </summary>
        public long MaxOffset
        {
            get
            {
                return _maxOffset;
            }
        }


        /// <summary>
        /// Get whether the page number calculated from the offset beyond the total page count.
        /// </summary>
        public bool IsBeyondPageCount
        {
            get
            {
                return _currentPageNo > _pageCount;
            }
        }

        /// <summary>
        /// Get whether the current page number does exist.
        /// </summary>
        public bool IsValidCurrentPageNo
        {
            get
            {
                return _currentPageNo > 0 && _currentPageNo <= _pageCount;
            }
        }

        /// <summary>
        /// Get whether the previous page number does exist.
        /// </summary>
        public bool IsValidPreviousPageNo
        {
            get
            {
                return _previousPageNo > 0 && _previousPageNo <= _pageCount;
            }
        }

        /// <summary>
        /// Get whether the next page number does exist.
        /// </summary>
        public bool IsValidNextPageNo
        {
            get
            {
                return _nextPageNo > 0 && _nextPageNo <= _pageCount;
            }
        }

        /// <summary>
        /// Get the number of records displayed per page.
        /// </summary>
        public long RecordsPerPage
        {
            get
            {
                return _recordsPerPage;
            }
        }

        /// <summary>
        /// Get the number of records offset for the current page.
        /// </summary>
        public long CurrentOffset
        {
            get
            {
                return _currentOffset;
            }
        }

        /// <summary>
        /// Get the total number of records available.
        /// </summary>
        public long TotalRecords
        {
            get
            {
                return _totalRecords;
            }
        }

        /// <summary>
        /// Get the number of records available on the current page.
        /// </summary>
        public long RecordsAvailableOnPage
        {
            get
            {
                return _recordsAvailableOnPage;
            }
        }

        /// <summary>
        /// Get the total number of pages available.
        /// </summary>
        public long PageCount
        {
            get
            {
                return _pageCount;
            }
        }

        /// <summary>
        /// Get the current page number.
        /// </summary>
        public long CurrentPageNo
        {
            get
            {
                return _currentPageNo;
            }
        }

        /// <summary>
        /// Get the next page number.
        /// </summary>
        public long NextPageNo
        {
            get
            {
                return _nextPageNo;
            }
        }

        /// <summary>
        /// Get the previous page number.
        /// </summary>
        public long PreviousPageNo
        {
            get
            {
                return _previousPageNo;
            }
        }

        /// <summary>
        /// Get the number of records offset for the next page.
        /// </summary>
        public long NextOffset
        {
            get
            {
                return _nextOffset;
            }
        }

        /// <summary>
        /// Get the number of records offset for the previous page.
        /// </summary>
        public long PreviousOffset
        {
            get
            {
                return _previousOffset;
            }
        }

        /// <summary>
        /// Get the first hit number of the current page.
        /// </summary>
        public long CurrentFirstHit
        {
            get
            {
                return _currentOffset + 1;
            }
        }

        /// <summary>
        /// Get the last hit number of the current page.
        /// </summary>
        public long CurrentLastHit
        {
            get
            {
                return _currentOffset + _recordsAvailableOnPage;
            }
        }

        /// <summary>
        /// Get the first hit number of the previous page.
        /// </summary>
        public long PreviousFirstHit
        {
            get
            {
                return _previousOffset + 1;
            }
        }

        /// <summary>
        /// Get the last hit number of the previous page.
        /// </summary>
        public long PreviousLastHit
        {
            get
            {
                return _currentOffset;
            }
        }

        /// <summary>
        /// Get the first hit number of the next page.
        /// </summary>
        public long NextFirstHit
        {
            get
            {
                return _currentOffset + _recordsPerPage + 1;
            }
        }

        /// <summary>
        /// Get the last hit number of the next page.
        /// </summary>
        public long NextLastHit
        {
            get
            {
                return Math.Min(_currentOffset + (2 * _recordsPerPage), _totalRecords);
            }
        }

        /// <summary>
        /// Get the number of the hits on the previous page.
        /// </summary>
        public long PreviousHits
        {
            get
            {
                return PreviousLastHit - PreviousFirstHit + 1;
            }
        }

        /// <summary>
        /// Get the number of the hits on the next page.
        /// </summary>
        public long NextHits
        {
            get
            {
                return NextLastHit - NextFirstHit + 1;
            }
        }

        /// <summary>
        /// Get the number of pages displayed per group
        /// </summary>
        public long PagesPerGroup
        {
            get
            {
                return _pagesPerGroup;
            }
        }

        /// <summary>
        /// Get the last page number in the previous page group
        /// </summary>
        public long PreviousGroupLastPageNo
        {
            get
            {
                return _previousGroupLastPageNo;
            }
        }

        /// <summary>
        /// Get the first page number in the next page group
        /// </summary>
        public long NextGroupFirstPageNo
        {
            get
            {
                return _nextGroupFirstPageNo;
            }
        }

        public long[] Pages
        {
            get
            {
                long nextGroupFirstPageNo = 0;
                long[] pages = null;

                if (IsValidCurrentPageNo)
                {
                    if (_pagesPerGroup == 0 || _nextGroupFirstPageNo == 0 || _nextGroupFirstPageNo > _pageCount)
                    {
                        nextGroupFirstPageNo = _pageCount;
                    }
                    else
                    {
                        nextGroupFirstPageNo = _nextGroupFirstPageNo - 1;
                    }

                    pages = new long[nextGroupFirstPageNo - _previousGroupLastPageNo];

                    for (long pageCount = 0; pageCount < pages.Length; pageCount++)
                    {
                        pages[pageCount] = pageCount + _previousGroupLastPageNo + 1;
                    }
                }
                else
                {
                    pages = new long[0];
                }

                return pages;
            }
        }

        #endregion
    }
}