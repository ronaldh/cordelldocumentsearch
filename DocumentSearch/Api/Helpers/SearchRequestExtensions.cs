﻿using DocumentSearch.Api.DataTransferObjects;

namespace DocumentSearch.Api.Helpers
{
    public static class SearchRequestExtensions
    {
        public static string GetAggregateFieldName(this SearchRequest searchRequest)
        {
            switch (searchRequest.Aggregate)
            {
                case Aggregation.Value:
                    return "Project.Value";
                case Aggregation.InteriorSqFt:
                    return "Project.FloorArea";
                case Aggregation.FootprintSqFt:
                    return "Project.SiteArea";
            }
            return null;
        }

        public static string GetSortFieldName(this SearchRequest searchRequest)
        {
            switch (searchRequest.Sort)
            {
                case SortBy.Date:
                    return "Project.BidDate";
                case SortBy.Value:
                case SortBy.TotalProjectValue:
                case SortBy.ComparativeProjectValue:
                    return "Project.Value";
                case SortBy.InteriorSqFt:
                case SortBy.TotalProjectInteriorSqFt:
                case SortBy.ComparativeProjectInteriorSqFt:
                    return "Project.FloorArea";
                case SortBy.FootprintSqFt:
                case SortBy.TotalProjectFootprintSqFt:
                case SortBy.ComparativeProjectFootprintSqFt:
                    return "Project.SiteArea";
            }
            return null;
        }

        public static bool IsSortedByCount(this SearchRequest searchRequest)
        {
            return searchRequest.GetSortFieldName() == null;
        }
    }
}