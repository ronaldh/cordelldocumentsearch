﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocumentSearch.Api.Helpers
{
    public interface IRange<T>
    {
        T Start { get; }
        T End { get; }
    }

    public class Range<T> : IRange<T>
    {
        public Range(T start, T end)
        {
            Start = start;
            End = end;
        }

        public T Start { get; private set; }
        public T End { get; private set; }
    }
}