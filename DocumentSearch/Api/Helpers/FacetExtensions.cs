﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DocumentSearch.Api.DataTransferObjects;

namespace DocumentSearch.Api.Helpers
{
    public static class FacetExtensions
    {
        /// <summary>
        /// FilterValues needs to be the format ["yyyy-MM", "yyyy-MM"]
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static Range<int> ToBidDateRange(this Facet facet)
        {
            if (facet == null || facet.FilterValues == null || facet.FilterValues.Count != 2)
            {
                return null;
            }
            var from = facet.FilterValues[0].Split('-');
            var to = facet.FilterValues[1].Split('-');
            var fromMonth = new DateTime(int.Parse(from[0]), int.Parse(from[1]), 1).MonthsSince20000101();
            var toMonth = new DateTime(int.Parse(to[0]), int.Parse(to[1]), 1).MonthsSince20000101();
            var range = new Range<int>(fromMonth, toMonth);
            return range;
        }

        /// <summary>
        /// FilterValues needs to be the format ["0", "1000000"]
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static Range<long> ToRange(this Facet facet)
        {
            if (facet == null || facet.FilterValues == null || facet.FilterValues.Count != 2)
            {
                return null;
            }
            var from = long.Parse(facet.FilterValues[0]);
            var to = long.Parse(facet.FilterValues[1]);
            var range = new Range<long>(from, to);
            return range;
        }
    }
}