﻿using DocumentSearch.Api.DataTransferObjects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace DocumentSearch.Api.Helpers
{
    public class FacetHelper
    {
        static public HashSet<string> DocumentSingleValuedFieldSet
        {
            get
            {
                return new HashSet<string>
                {
                    "Project.PSA"
                };
            }
        }
        static public HashSet<string> DocumentFieldSet
        {
            get
            {
                return new HashSet<string>(DocumentSingleValuedFieldSet);
            }
        }
        static public HashSet<string> AllFieldSet
        {
            get
            {
                return new HashSet<string>(ProjectFieldSet.Concat(DocumentFieldSet));
            }
        }
        static public HashSet<string> ProjectSingleValuedFieldSet
        {
            get
            {
                return new HashSet<string>
                {
                    "Project.BidDateMonths",
                    "Project.OwnerType_facet",
                    "Project.WorkType_facet",
                    "Project.Value"
                };
            }
        }
        static public HashSet<string> ProjectMultiValuedFieldSet
        {
            get
            {
                return new HashSet<string>
                {
                    "Company.Geography",
                    "Project.CompanyRoleClassificationTypes_facet",
                    "Project.DocumentCSICodesShort",
                    "Project.Geography",
                    "Project.Material_facet",
                    "Project.SubCategory_facet"
                };
            }
        }
        static public FacetSettingCollection FacetSettingCollection
        {
            get
            {
                return new FacetSettingCollection
                {
                    new FacetSetting("Company.Geography", "2|UNITED STATES|", 1, 3),
                    new FacetSetting("Project.CompanyRoleClassificationTypes_facet", "2|ROOT|", 2, 3),
                    new FacetSetting("Project.Geography", "2|UNITED STATES|", 1, 3),
                    new FacetSetting("Project.Material_facet", "1|", 1, 2),
                    new FacetSetting("Project.SubCategory_facet", "1|", 1, 2)
                };
            }
        }
        static public HashSet<string> ProjectFieldSet
        {
            get
            {
                return new HashSet<string>(ProjectSingleValuedFieldSet.Concat(ProjectMultiValuedFieldSet));
            }
        }
        /// <summary>
        /// Fields from Mozart that need to be filtered in MozartPlus, but doesn't need facets to be displayed
        /// </summary>
        static public HashSet<string> MozartOnlyFieldSet
        {
            get
            {
                return new HashSet<string>
                {
                    "Company.CountyName",
                    "Company.RoleClassificationParentType",
                    "Project.BidDate",
                    "Project.CountyName_facet",
                    "Project.CreateDate",
                    "Project.ModifyDate",
                    "Project.PostalCode",
                    "Project.StageType_facet"
                };
            }
        }
    }

}