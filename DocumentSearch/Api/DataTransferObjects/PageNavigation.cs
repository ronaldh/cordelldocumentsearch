﻿using System.Collections.Generic;

namespace DocumentSearch.Api.DataTransferObjects
{
    public class Page
    {
        public long Number { get; set; }
        public long Offset { get; set; }
    }

    public partial class PageNavigation
    {
        public Page PreviousPage { get; set; }
        public Page CurrentPage { get; set; }
        public Page NextPage { get; set; }
        public List<Page> Pages { get; set; }
        public long TotalPages { get; set; }
        public long TotalHits { get; set; }
        public long RowsPerPage { get; set; }
        public long PreviousGroupPageNo { get; set; }
        public long NextGroupPageNo { get; set; }
        public long PagesPerGroup { get; set; }
    }
}