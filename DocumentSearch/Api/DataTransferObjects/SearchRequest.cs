﻿using System;
using System.Collections.Generic;

namespace DocumentSearch.Api.DataTransferObjects
{
    public enum Aggregation
    {
        Count = 0,
        Value = 1,
        InteriorSqFt = 2,
        FootprintSqFt = 3
    }

    public enum SortBy
    {
        Date = 0,
        Value = 1,
        InteriorSqFt = 2,
        FootprintSqFt = 3,
        TotalProjectCount = 4,
        TotalProjectValue = 5,
        TotalProjectInteriorSqFt = 6,
        TotalProjectFootprintSqFt = 7,
        ComparativeProjectCount = 8,
        ComparativeProjectValue = 9,
        ComparativeProjectInteriorSqFt = 10,
        ComparativeProjectFootprintSqFt = 11
    }

    public enum TypeOfResult
    {
        Projects = 0,
        Companies = 1,
        Documents = 2,
        None = 99
    }

    public enum TypeOfFilter
    {
        Documents = 1,
        Companies = 2
    }

    public class SearchRequestQuery
    {
        public string Name { get; set; }
        public string Label { get; set; }
        public string Color { get; set; }
        public string Query { get; set; }
        public TypeOfFilter FilterType { get; set; }
    }

    public class Facet
    {
        public string Name { get; set; }
        public List<string> FilterValues { get; set; }
    }

    public class SearchRequest 
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid UserId { get; set; }
        public List<SearchRequestQuery> Queries { get; set; }
        public Aggregation Aggregate { get; set; }
        public int Start { get; set; }
        public int RowsPerPage { get; set; }
        public TypeOfResult ResultType { get; set; }
        public SortBy Sort { get; set; }
        public List<Facet> Filters { get; set; }
    }
}