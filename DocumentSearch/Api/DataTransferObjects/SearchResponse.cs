﻿using DocumentSearch.Api.Configuration;
using DocumentSearch.Api.Helpers;
using DocumentSearch.Api.Queries;
using SolrSearchAPI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace DocumentSearch.Api.DataTransferObjects
{
    public class SearchResponse
    {
        public List<Result> ResultList { get; set; }
        public List<string> MatchingQueryTitles { get; set; }
        public List<FacetResult> Facets { get; set; }
        public PageNavigation PageNavigation { get; set; }
        public Stats Statistics { get; set; }

        public SearchResponse(ResultCollection resultCollection, SearchRequest originalRequest)
        {
            var stopwatch = Stopwatch.StartNew();
            ResultList = new List<Result>();
            Statistics = new Stats();
            PageNavigation = new PageNavigation();
            MatchingQueryTitles = new List<string>();
            switch (originalRequest.ResultType)
            {
                case TypeOfResult.Projects:
                    ExtractProjectResults(
                        resultCollection.Where(query => query.TypeOfQuery == TypeOfQuery.Project),
                        originalRequest);
                    break;
                case TypeOfResult.Companies:
                    MatchingQueryTitles = resultCollection.Where(query => query.TypeOfQuery == TypeOfQuery.Company).Select(d => d.QueryName).ToList();
                    ExtractCompanyResults(
                    resultCollection.Where(query => query.TypeOfQuery == TypeOfQuery.Company),
                    originalRequest);
                    break;
                case TypeOfResult.Documents:
                    ExtractDocumentResults(
                        resultCollection.Where(query => query.TypeOfQuery == TypeOfQuery.Document),
                        originalRequest);
                    break;
                default:
                    break;
            }
            stopwatch.Stop();
            Statistics.Trace.Add("SearchResponse constructor (ms): " + stopwatch.ElapsedMilliseconds);
        }


        private static DateTime _epochDateTime = new DateTime(2000, 1, 1);


        private static void MapFacetFields(SearchRequest originalRequest, FacetResultCollection facetCollection, ISearchQuery query)
        {
            var solrResult = query.SolrResult;
            var queryName = query.QueryName;
            if (solrResult.facets == null ||
                solrResult.facets.facet_fields == null)
            {
                return;
            }
            foreach (var facetField in solrResult.facets.facet_fields)
            {
                FacetResult facet;
                var facetFieldName = facetField.Key;
                var facetValueAndStatValueDictionary = facetField.Value.Facet;
                var facetExists = facetCollection.Contains(facetFieldName);
                if (facetExists)
                {
                    facet = facetCollection[facetFieldName];
                }
                else
                {
                    facet = new FacetResult()
                    {
                        Name = facetFieldName,
                        Columns = new List<string>(),
                        Entries = new List<FacetEntry>()
                    };
                    if (FacetHelper.FacetSettingCollection.Contains(facetFieldName))
                    {
                        facet.Setting = FacetHelper.FacetSettingCollection[facetFieldName];
                    }
                    facetCollection.Add(facet);
                    if (facetFieldName == "Document.Type")
                    {
                        // matching documents tile always shows count
                        facet.Columns.Add(Aggregation.Count.ToString());
                    }
                    else
                    {
                        facet.Columns.Add(originalRequest.Aggregate.ToString());
                    }
                }
                facet.Columns.Add(queryName);

                foreach (var facetValueAndStatValue in facetValueAndStatValueDictionary)
                {
                    var facetValue = facetValueAndStatValue.Key;
                    long statValue = facetValueAndStatValue.Value;

                    if (facetExists)
                    {
                        var matchingRow = facet.GetRow(facetValue);
                        // case where prior query results didn't contain this facet value
                        if (matchingRow == null)
                        {
                            matchingRow = new FacetEntry()
                            {
                                Value = facetValue,
                                Aggregations = new List<long>(facet.Columns.Count)
                            };
                            for (int columnIndex = 2; columnIndex < facet.Columns.Count; columnIndex++)
                            {
                                matchingRow.Aggregations.Add(0);
                            }
                            facet.Entries.Add(matchingRow);
                        }
                        matchingRow.Aggregations.Add(statValue);
                    }
                    else
                    {
                        facet.Entries.Add(new FacetEntry()
                        {
                            Value = facetValue,
                            Aggregations = new List<long>() { statValue }
                        });
                    }
                }

                if (facetExists)
                {
                    // make sure missing facet cells default to 0
                    foreach (var row in facet.Entries)
                    {
                        var columnsToAdd = facet.Columns.Count - row.Aggregations.Count - 1;
                        for (int columnCount = 0; columnCount < columnsToAdd; columnCount++)
                        {
                            row.Aggregations.Add(0);
                        }
                    }
                }
            }
        }

        private static void MapFacetRanges(SearchRequest originalRequest, FacetResultCollection gridCollection, ISearchQuery query)
        {
            var solrResult = query.SolrResult;
            var queryName = query.QueryName;
            if (solrResult.facets == null ||
                solrResult.facets.facet_ranges == null)
            {
                return;
            }
            foreach (var facetField in solrResult.facets.facet_ranges)
            {
                FacetResult facet;
                var facetFieldName = facetField.Key;
                var facetValueAndStatValueDictionary = facetField.Value.counts.Facet;
                var gridExists = gridCollection.Contains(facetFieldName);
                if (gridExists)
                {
                    facet = gridCollection[facetFieldName];
                }
                else
                {
                    facet = new FacetResult()
                    {
                        Name = facetFieldName,
                        Columns = new List<string>(),
                        Entries = new List<FacetEntry>()
                    };
                    if (FacetHelper.FacetSettingCollection.Contains(facetFieldName))
                    {
                        facet.Setting = FacetHelper.FacetSettingCollection[facetFieldName];
                    }
                    gridCollection.Add(facet);
                    facet.Columns.Add(originalRequest.Aggregate.ToString());
                }
                facet.Columns.Add(queryName);

                foreach (var facetValueAndStatValue in facetValueAndStatValueDictionary)
                {
                    var facetValue = facetValueAndStatValue.Key;
                    long statValue = facetValueAndStatValue.Value;

                    if (facetFieldName == "Project.Value")
                    {
                        var projectValue = double.Parse(facetValue);
                        // prevent 0 from being added
                        if (projectValue == 0)
                        {
                            continue;
                        }
                        facetValue = (projectValue / 1000000).ToString();
                        if (!facetValue.Contains('.'))
                        {
                            facetValue += "M";
                        }
                    }

                    if (gridExists)
                    {
                        var matchingRow = facet.GetRow(facetValue);
                        // case where prior query results didn't contain this facet value
                        if (matchingRow == null)
                        {
                            matchingRow = new FacetEntry()
                            {
                                Value = facetValue,
                                Aggregations = new List<long>(facet.Columns.Count)
                            };
                            for (int columnIndex = 2; columnIndex < facet.Columns.Count; columnIndex++)
                            {
                                matchingRow.Aggregations.Add(0);
                            }
                            facet.Entries.Add(matchingRow);
                        }
                        matchingRow.Aggregations.Add(statValue);
                    }
                    else
                    {
                        facet.Entries.Add(new FacetEntry()
                        {
                            Value = facetValue,
                            Aggregations = new List<long>() { statValue }
                        });
                    }
                }

                if (gridExists)
                {
                    // make sure missing grid cells default to 0
                    foreach (var row in facet.Entries)
                    {
                        var columnsToAdd = facet.Columns.Count - row.Aggregations.Count - 1;
                        for (int columnCount = 0; columnCount < columnsToAdd; columnCount++)
                        {
                            row.Aggregations.Add(0);
                        }
                    }
                }
            }
        }

        private static void MapStatistics(SearchRequest originalRequest, FacetResultCollection gridCollection, ISearchQuery query)
        {
            var solrResult = query.SolrResult;
            var queryName = query.QueryName;
            if (solrResult.stats == null ||
                solrResult.stats.stats_fields == null ||
                solrResult.stats.stats_fields.Count == 0)
            {
                return;
            }
            foreach (var facetField in solrResult.stats.stats_fields.First().Value.facets)
            {
                FacetResult facet;
                var facetFieldName = facetField.Key;
                var facetValueAndStatValueDictionary = facetField.Value.Facet;
                var facetFieldFilter = originalRequest.Filters.Find(currentFilter => currentFilter.Name == facetFieldName);
                var gridExists = gridCollection.Contains(facetFieldName);
                if (gridExists)
                {
                    facet = gridCollection[facetFieldName];
                }
                else
                {
                    facet = new FacetResult()
                    {
                        Name = facetFieldName,
                        Columns = new List<string>(),
                        Entries = new List<FacetEntry>()
                    };
                    if (FacetHelper.FacetSettingCollection.Contains(facetFieldName))
                    {
                        facet.Setting = FacetHelper.FacetSettingCollection[facetFieldName];
                    }
                    gridCollection.Add(facet);
                    facet.Columns.Add(originalRequest.Aggregate.ToString());
                }
                facet.Columns.Add(queryName);

                foreach (var facetValueAndStatValue in facetValueAndStatValueDictionary)
                {
                    var facetValue = facetValueAndStatValue.Key;
                    var statValue = (long)facetValueAndStatValue.Value.sum;

                    // limit facets returned (StatsComponent and AnalyticsComponent does not support specifying prefixes like facets)
                    //if (string.IsNullOrEmpty(facetValue) || (grid.Prefix != null && !facetValue.StartsWith(grid.Prefix) && facetFieldFilter == null ))
                    if (string.IsNullOrEmpty(facetValue))
                    {
                        continue;
                    }

                    if (gridExists)
                    {
                        var matchingRow = facet.GetRow(facetValue);
                        // case where prior query results didn't contain this facet value
                        if (matchingRow == null)
                        {
                            matchingRow = new FacetEntry()
                            {
                                Value = facetValue,
                                Aggregations = new List<long>(facet.Columns.Count)
                            };
                            for (var columnIndex = 2; columnIndex < facet.Columns.Count; columnIndex++)
                            {
                                matchingRow.Aggregations.Add(0);
                            }
                            facet.Entries.Add(matchingRow);
                        }
                        matchingRow.Aggregations.Add(statValue);
                    }
                    else
                    {
                        facet.Entries.Add(new FacetEntry()
                        {
                            Value = facetValue,
                            Aggregations = new List<long>() { statValue }
                        });
                    }
                }

                if (gridExists)
                {
                    // make sure missing grid cells default to 0
                    foreach (var row in facet.Entries)
                    {
                        var columnsToAdd = facet.Columns.Count - row.Aggregations.Count - 1;
                        for (var columnCount = 0; columnCount < columnsToAdd; columnCount++)
                        {
                            row.Aggregations.Add(0);
                        }
                    }
                }
            }
        }

        private void ExtractPageNavigation(IEnumerable<ISearchQuery> queries, SearchRequest originalRequest)
        {
            
            var pagination = new Pagination(originalRequest.RowsPerPage,
                originalRequest.Start,
                queries.Count() > 0 ?  queries.Max(query => query.SolrResult.response.numFound) : 0,
                10,
                500);
            pagination.Calculate();

            PageNavigation.TotalPages = pagination.PageCount;
            PageNavigation.TotalHits = pagination.TotalRecords;

            if (pagination.IsValidPreviousPageNo)
            {
                PageNavigation.PreviousPage = new Page()
                {
                    Number = pagination.PreviousPageNo,
                    Offset = pagination.PreviousOffset
                };
            }

            if (pagination.IsValidCurrentPageNo)
            {
                PageNavigation.CurrentPage = new Page()
                {
                    Number = pagination.CurrentPageNo,
                    Offset = pagination.CurrentOffset
                };
            }

            if (pagination.IsValidNextPageNo && (pagination.NextOffset <= pagination.MaxOffset))
            {
                PageNavigation.NextPage = new Page()
                {
                    Number = pagination.NextPageNo,
                    Offset = pagination.NextOffset
                };
            }

            if (pagination.PagesPerGroup > 0 && pagination.IsValidCurrentPageNo)
            {
                PageNavigation.PreviousGroupPageNo = pagination.PreviousGroupLastPageNo;

                if (pagination.IsValidNextGroup())
                {
                    PageNavigation.NextGroupPageNo = pagination.NextGroupFirstPageNo;
                }

                PageNavigation.PagesPerGroup = pagination.PagesPerGroup;
            }

            PageNavigation.RowsPerPage = originalRequest.RowsPerPage;
            PageNavigation.Pages = new List<Page>();

            foreach (var pageNo in pagination.Pages)
            {
                if (pagination.IsValidPageOffset(pageNo))
                {
                    var page = new Page()
                    {
                        Number = pageNo,
                        Offset = pagination.GetOffset(pageNo)
                    };
                    PageNavigation.Pages.Add(page);
                }
            }
        }

        private void ExtractProjectResults(IEnumerable<ISearchQuery> queries, SearchRequest originalRequest)
        {
            var idToDocDictionary = new Dictionary<string, Doc>();
            var idToQueriesDictionary = new Dictionary<string, HashSet<string>>();

            foreach (var query in queries)
            {
                foreach (var doc in query.SolrResult.response.docs)
                {
                    var id = doc.GetFieldAsSingleString("Project.ProjectId");
                    if (!idToDocDictionary.ContainsKey(id))
                    {
                        idToDocDictionary.Add(id, doc);
                    }
                    if (!idToQueriesDictionary.ContainsKey(id))
                    {
                        idToQueriesDictionary.Add(id, new HashSet<string>());
                    }
                    if (!idToQueriesDictionary[id].Contains(query.QueryName))
                    {
                        idToQueriesDictionary[id].Add(query.QueryName);
                    }
                }
            }

            ExtractPageNavigation(queries, originalRequest);

            IOrderedEnumerable<KeyValuePair<string, Doc>> orderedEnumerable = null;

            switch (originalRequest.Sort)
            {
                case SortBy.Date:
                    orderedEnumerable = idToDocDictionary
                        .OrderByDescending(kv => kv.Value.GetFieldAsSingleDateTime("Project.BidDate"));
                    break;
                case SortBy.Value:
                    orderedEnumerable = idToDocDictionary
                        .OrderByDescending(kv => kv.Value.GetFieldAsSingleDouble("Project.Value"));
                    break;
                case SortBy.InteriorSqFt:
                    orderedEnumerable = idToDocDictionary
                        .OrderByDescending(kv => kv.Value.GetFieldAsSingleInt("Project.FloorArea"));
                    break;
                case SortBy.FootprintSqFt:
                    orderedEnumerable = idToDocDictionary
                        .OrderByDescending(kv => kv.Value.GetFieldAsSingleInt("Project.SiteArea"));
                    break;
            }

            foreach (var kv in orderedEnumerable
                .Skip(originalRequest.Start)
                .Take(originalRequest.RowsPerPage))
            {
                var hit = kv.Value;

                var id = hit.GetFieldAsSingleString("Project.ProjectId");
                var siteArea = hit.GetFieldAsSingleInt("Project.SiteArea");
                var floorArea = hit.GetFieldAsSingleInt("Project.FloorArea");
                var projectValue = hit.GetFieldAsSingleDouble("Project.Value");

                string sortValue = string.Empty;
                switch (originalRequest.Sort)
                {
                    case SortBy.Date:
                        var bidDate = hit.GetFieldAsSingleDateTime("Project.BidDate");
                        if (bidDate.HasValue)
                        {
                            sortValue = bidDate.Value.ToString("yyyy-MM-dd");
                        }
                        break;
                    case SortBy.Value:
                        if (projectValue.HasValue)
                        {
                            sortValue = projectValue.Value.ToString();
                        }
                        break;
                    case SortBy.InteriorSqFt:
                        if (floorArea.HasValue)
                        {
                            sortValue = floorArea.Value.ToString();
                        }
                        break;
                    case SortBy.FootprintSqFt:
                        if (siteArea.HasValue)
                        {
                            sortValue = siteArea.Value.ToString();
                        }
                        break;
                }
                string aggregate = string.Empty;
                switch (originalRequest.Aggregate)
                {
                    case Aggregation.Count:
                        aggregate = "1";
                        break;
                    case Aggregation.FootprintSqFt:
                        if (siteArea.HasValue)
                        {
                            aggregate = siteArea.Value.ToString();
                        }
                        break;
                    case Aggregation.InteriorSqFt:
                        if (floorArea.HasValue)
                        {
                            aggregate = floorArea.Value.ToString();
                        }
                        break;
                    case Aggregation.Value:
                        if (projectValue.HasValue)
                        {
                            aggregate = projectValue.Value.ToString();
                        }
                        break;
                }
                ResultList.Add(new Result()
                {
                    Id = id,
                    Name = hit.GetFieldAsSingleString("Project.Title"),
                    Url = new Uri(DocumentSearchConfiguration.Current().CordellConfiguration
                        .ProjectDetailUriString.Replace("{ProjectId}", id)),
                });

            }
        }

        private void ExtractDocumentResults(IEnumerable<ISearchQuery> queries, SearchRequest originalRequest)
        {
            var idToDocDictionary = new Dictionary<string, Doc>();
            var idToQueriesDictionary = new Dictionary<string, HashSet<string>>();

            foreach (var query in queries)
            {
                foreach (var doc in query.SolrResult.response.docs)
                {
                    var id = doc.GetFieldAsSingleString("id");
                    if (!idToDocDictionary.ContainsKey(id))
                    {
                        if (query.SolrResult.highlighting.ContainsKey(id))
                        {
                            doc.Fields.Add("highlight", new List<string>() { query.SolrResult.highlighting[id] });
                        }
                        idToDocDictionary.Add(id, doc);
                    }
                    if (!idToQueriesDictionary.ContainsKey(id))
                    {
                        idToQueriesDictionary.Add(id, new HashSet<string>());
                    }
                    if (!idToQueriesDictionary[id].Contains(query.QueryName))
                    {
                        idToQueriesDictionary[id].Add(query.QueryName);
                    }
                }
                //TODO: add highlighting, requires extensing SolrResult
            }
            IOrderedEnumerable<KeyValuePair<string, Doc>> orderedEnumerable = null;

            switch (originalRequest.Sort)
            {
                //case SortBy.Date:
                //    orderedEnumerable = idToDocDictionary
                //        .OrderByDescending(kv => kv.Value.GetFieldAsSingleDateTime("Project.BidDate"));
                //    break;
                //case SortBy.Value:
                //    orderedEnumerable = idToDocDictionary
                //        .OrderByDescending(kv => kv.Value.GetFieldAsSingleDouble("Project.Value"));
                //    break;
                //case SortBy.InteriorSqFt:
                //    orderedEnumerable = idToDocDictionary
                //        .OrderByDescending(kv => kv.Value.GetFieldAsSingleInt("Project.FloorArea"));
                //    break;
                //case SortBy.FootprintSqFt:
                //    orderedEnumerable = idToDocDictionary
                //        .OrderByDescending(kv => kv.Value.GetFieldAsSingleInt("Project.SiteArea"));
                //    break;
                default:
                    orderedEnumerable = idToDocDictionary
                        .OrderByDescending(kv => kv.Value.GetFieldAsSingleDouble("score"));
                    break;
            }

            foreach (var kv in orderedEnumerable
                .Skip(originalRequest.Start)
                .Take(originalRequest.RowsPerPage))
            {
                var hit = kv.Value;

                var id = hit.GetFieldAsSingleString("Project.ProjectId");
                var siteArea = hit.GetFieldAsSingleInt("Project.SiteArea");
                var floorArea = hit.GetFieldAsSingleInt("Project.FloorArea");
                var projectValue = hit.GetFieldAsSingleDouble("Project.Value");

                string sortValue = string.Empty;
                switch (originalRequest.Sort)
                {
                    case SortBy.Date:
                        var bidDate = hit.GetFieldAsSingleDateTime("Project.BidDate");
                        if (bidDate.HasValue)
                        {
                            sortValue = bidDate.Value.ToString("yyyy-MM-dd");
                        }
                        break;
                    case SortBy.Value:
                        if (projectValue.HasValue)
                        {
                            sortValue = projectValue.Value.ToString();
                        }
                        break;
                    case SortBy.InteriorSqFt:
                        if (floorArea.HasValue)
                        {
                            sortValue = floorArea.Value.ToString();
                        }
                        break;
                    case SortBy.FootprintSqFt:
                        if (siteArea.HasValue)
                        {
                            sortValue = siteArea.Value.ToString();
                        }
                        break;
                }
                string aggregate = string.Empty;
                switch (originalRequest.Aggregate)
                {
                    case Aggregation.Count:
                        aggregate = "1";
                        break;
                    case Aggregation.FootprintSqFt:
                        if (siteArea.HasValue)
                        {
                            aggregate = siteArea.Value.ToString();
                        }
                        break;
                    case Aggregation.InteriorSqFt:
                        if (floorArea.HasValue)
                        {
                            aggregate = floorArea.Value.ToString();
                        }
                        break;
                    case Aggregation.Value:
                        if (projectValue.HasValue)
                        {
                            aggregate = projectValue.Value.ToString();
                        }
                        break;
                }
                var result = new Result()
                {
                    Id = id,
                    Name = hit.GetFieldAsSingleString("Document.FilePath"),
                    Url = new Uri(DocumentSearchConfiguration.Current().CordellConfiguration
                        .ProjectDetailUriString.Replace("{ProjectId}", id)),
                    Snippet = hit.GetFieldAsSingleString("content")
                };
                if (hit.Fields.ContainsKey("highlight"))
                {
                    result.Snippet = hit.GetFieldAsSingleString("highlight");
                }
                ResultList.Add(result);

            }
        }

        public void AddTrace(ISearchQuery query)
        {
            Statistics.Trace.Add("Query " + query.QueryId + " (ms): " + query.SolrResult.responseHeader.QTime + " " + query.SolrResult.responseHeader.cqtime + " " + query.SolrResult.responseHeader.dtime + "<br/>" +
    "<a target=\"blank\" href=\"" + query.SolrResult.responseHeader.queryurl.Replace("wt=json", "wt=xml") + "\">XML</a> " +
    "<a target=\"blank\" href=\"" + query.SolrResult.responseHeader.queryurl + "\">" + HttpUtility.UrlDecode(query.SolrResult.responseHeader.queryurl) + "</a>");
        }

        public void AddCompanyDetails(ISearchQuery companyDetails, SearchRequest originalRequest)
        {
            var stopwatch = Stopwatch.StartNew();
            foreach (var hit in companyDetails.SolrResult.grouped.Values.First().doclist.docs)
            {
                var id = hit.GetFieldAsSingleString("Company.CompanyId");
                var result = ResultList.Find(r => r.Id == id);
                if (result != null)
                {
                    result.Name = hit.GetFieldAsSingleString("Company.CompanyName");
                }
                else
                {
                    throw new Exception("Expected company id not found");
                }
            }
            stopwatch.Stop();
            Statistics.Trace.Add("SearchResponse AddCompanyDetails (ms): " + stopwatch.ElapsedMilliseconds);
        }

        private void ExtractCompanyResults(IEnumerable<ISearchQuery> queries, SearchRequest originalRequest)
        {
            var resultDictionary = new Dictionary<string, Result>();
            foreach (var query in queries)
            {
                var solrResult = query.SolrResult;

                if (solrResult.stats == null)
                {
                    continue;
                }
                var sortFieldName = originalRequest.GetSortFieldName();
                var facetValueAndStatValueDictionary = solrResult.stats.stats_fields[sortFieldName].facets[CompanyQuery.CompanyIdFacetField].Facet;
                foreach (var facetValueAndStatValue in facetValueAndStatValueDictionary)
                {
                    if (!facetValueAndStatValue.Key.Contains(CompanyQuery.CompanyRootPrefix))
                    {
                        continue;
                    }
                    var id = facetValueAndStatValue.Key.Replace(CompanyQuery.CompanyRootPrefix, string.Empty);
                    long statValue = (long)facetValueAndStatValue.Value.sum;

                    if (!resultDictionary.ContainsKey(id))
                    {
                        var result = new Result()
                        {
                            Id = id,
                            MatchingQueries = new List<string>()
                        };
                        for (var index = 0; index < queries.Count(); index++)
                        {
                            result.MatchingQueries.Add("0");
                        }
                        resultDictionary.Add(id, result);
                    }
                    resultDictionary[id].MatchingQueries[MatchingQueryTitles.IndexOf(query.QueryName)] = statValue.ToString();
                }

            }

            foreach (var result in resultDictionary.Values)
            {
                if (originalRequest.Sort.ToString().StartsWith("Comparative"))
                {
                    var initial = long.Parse(result.MatchingQueries[0]);
                    var sumOfRest = result.MatchingQueries
                        .GetRange(1, result.MatchingQueries.Count - 1)
                        .Sum(d => long.Parse(d));
                    result.SortValue =  Math.Abs(initial - sumOfRest).ToString();
                }
                else
                {
                    result.SortValue = result.MatchingQueries
                        .Sum(d => long.Parse(d)).ToString();
                }
            }

            foreach (var result in resultDictionary
                .OrderByDescending(i => long.Parse(i.Value.SortValue))
                .Take(originalRequest.RowsPerPage))
            {
                result.Value.Url = new Uri(DocumentSearchConfiguration.Current().CordellConfiguration
                    .CompanyDetailUriString.Replace("{CompanyId}", result.Value.Id));
                ResultList.Add(result.Value);
            }
        }
    }
}