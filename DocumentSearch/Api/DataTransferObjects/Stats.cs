﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocumentSearch.Api.DataTransferObjects
{
    public class Stats
    {
        public Stats()
        {
            Trace = new List<string>();
        }
        public long Universe { get; set; }
        public long Filtered { get; set; }
        public List<string> Trace { get; set; }
    }
}