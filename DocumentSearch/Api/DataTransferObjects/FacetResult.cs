﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace DocumentSearch.Api.DataTransferObjects
{
    public class FacetResult
    {
        private FacetValueCollection _collection;

        public string Name { get; set; }
        public List<string> Columns { get; set; }
        public FacetSetting Setting { get; set; }
        public IList<FacetEntry> Entries
        {
            get
            {
                if (_collection == null)
                {
                    _collection = new FacetValueCollection();
                }
                return _collection;
            }

            set
            {
                _collection = new FacetValueCollection(value);
            }
        }
        public FacetEntry GetRow(string rowValue)
        {
            if (_collection.Contains(rowValue))
            {
                return _collection[rowValue];
            }
            return null;
        }
    }


    /// <summary>
    /// A FacetEntry represents the result of a single facet, 
    /// Each facet 
    /// </summary>
    public class FacetEntry
    {
        public string Value { get; set; }
        public List<long> Aggregations { get; set; }
    }
    public class FacetValueCollection : KeyedCollection<string, FacetEntry>
    {
        public FacetValueCollection()
        {
        }

        public FacetValueCollection(IEnumerable<FacetEntry> rows)
        {
            foreach (var row in rows)
            {
                Add(row);
            }
        }

        protected override string GetKeyForItem(FacetEntry item)
        {
            return item.Value;
        }
    }

    public class FacetResultCollection : KeyedCollection<string, FacetResult>
    {
        protected override string GetKeyForItem(FacetResult item)
        {
            return item.Name;
        }
    }

    public class FacetSetting
    {
        public string Name { get; set; }
        public string FirstLevelPrefix { get; set; }
        public int FirstLevel { get; set; }
        public int AvailableLevels { get; set; }

        public FacetSetting(string name, string firstLevelPrefix, int firstLevel, int availableLevels)
        {
            Name = name;
            FirstLevelPrefix = firstLevelPrefix;
            FirstLevel = firstLevel;
            AvailableLevels = availableLevels;
        }
    }

    public class FacetSettingCollection : KeyedCollection<string, FacetSetting>
    {
        protected override string GetKeyForItem(FacetSetting item)
        {
            return item.Name;
        }
    }
}