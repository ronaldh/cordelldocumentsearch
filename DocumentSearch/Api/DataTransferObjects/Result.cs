﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocumentSearch.Api.DataTransferObjects
{
    public class Result
    {
        public string Id { get; set; }
        public Uri Url { get; set; }
        public string Name { get; set; }
        public string SortValue { get; set; }
        public string Aggregate { get; set; }
        public List<string> MatchingQueries { get; set; }
        public bool Specs { get; set; }
        public string Snippet { get; set; }
    }
}