﻿using DocumentSearch.Api.DataTransferObjects;
using DocumentSearch.Api.Helpers;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DocumentSearch.Api.Queries
{
    /// <summary>
    /// Use this query to get facets on companies to allow count, sorting and statistics.
    /// Must be used in conjunction with a CompanyResultQuery to get the actual company results
    /// </summary>
    public class CompanyQuery : BaseQuery
    {
        public static string CompanyIdFacetField = "Project.CompanyIDs";
        public static string CompanyRootPrefix = "1|ROOT|";

        public CompanyQuery(SearchRequestQuery requestQuery, SearchRequest originalRequest, Uri solrUri = null) :
            base(TypeOfQuery.Company, requestQuery, originalRequest)
        {
            if (originalRequest.ResultType != TypeOfResult.Companies)
            {
                throw new ArgumentException(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString() + " should only be called with the aim of retrieving a company result type, was: " + originalRequest.ResultType);
            }
            SolrUri = solrUri;

            if (SolrUri != null)
            {
                // todo: improve response from aspire, too many stats facet values causes slowness
                RequestTimeoutInMilliseconds = 20000;
            }

            _options.Start = 0;
            _options.Rows = 0;
            _options.Facet.Limit = 160;

            AddFilterTypeFilters();
            _options.FilterQueries.Add(new SolrQueryByField("ContentType", "Project"));
            AddProjectFacetsWithFacetFilters();
        }

        internal override void AddProjectFacetsWithFacetFilters()
        {
            var aggregateFieldName = _originalRequest.GetAggregateFieldName();
            if (_originalRequest.IsSortedByCount())
            {
                var facet = new SolrFacetFieldQuery(CompanyIdFacetField);
                facet.Prefix = CompanyRootPrefix;
                facet.Sort = true;
                _options.AddFacets(facet);

                _options.AddFacets(GetProjectValueFacetQuery());

                if (_originalRequest.Aggregate == Aggregation.Count)
                {
                    base.AddProjectFacetsWithFacetFilters();
                }
                else
                {
                    var facetFieldNames = FacetHelper.ProjectSingleValuedFieldSet.Where(fieldName => fieldName != "Project.Value").Except(_originalRequest.Filters.Select(filter => filter.Name));
                    _options.Stats = new StatsParameters();
                    _options.Stats.AddFieldWithFacets(aggregateFieldName, facetFieldNames);
                }
            }
            else
            {
                var sortFieldName = _originalRequest.GetSortFieldName();

                _options.Stats = new StatsParameters();

                if (_originalRequest.Aggregate == Aggregation.Count)
                {
                    _options.Stats.AddFieldWithFacets(sortFieldName, CompanyIdFacetField);
                }
                else
                {
                    if (sortFieldName == aggregateFieldName)
                    {
                        var facetFieldNames = new List<string>();
                        facetFieldNames.Add(CompanyIdFacetField);
                        facetFieldNames.AddRange(FacetHelper.ProjectMultiValuedFieldSet.Except(_originalRequest.Filters.Select(filter => filter.Name)));
                        _options.Stats.AddFieldWithFacets(sortFieldName, facetFieldNames);
                    }
                    else
                    {
                        _options.Stats.AddFieldWithFacets(sortFieldName, CompanyIdFacetField);
                        _options.Stats.AddFieldWithFacets(aggregateFieldName, FacetHelper.ProjectMultiValuedFieldSet.Except(_originalRequest.Filters.Select(filter => filter.Name)));
                    }
                }
            }
            AddProjectFacetFilters();
        }
    }
}