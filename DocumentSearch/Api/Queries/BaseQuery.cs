﻿using DocumentSearch.Api.DataTransferObjects;
using DocumentSearch.Api.Helpers;
using SolrNet;
using SolrSearchAPI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DocumentSearch.Api.Queries
{
    public abstract class BaseQuery : ISearchQuery
    {
        internal SearchRequestQuery _requestQuery;
        internal SolrNet.ISolrQuery _query;
        internal SolrNet.Commands.Parameters.ExtendedQueryOptions _options;
        internal SearchRequest _originalRequest;
        internal string _queryIdSuffix;

        public BaseQuery(TypeOfQuery typeOfQuery, SearchRequestQuery requestQuery, SearchRequest originalRequest, string queryIdSuffix = null)
        {
            _requestQuery = requestQuery;
            TypeOfQuery = typeOfQuery;
            _queryIdSuffix = queryIdSuffix;
            _query = new SolrNet.SolrQuery("*:*");
            _originalRequest = originalRequest;
            _options = new SolrNet.Commands.Parameters.ExtendedQueryOptions();
            _options.ExtraParams = new[]
                {
                    new KeyValuePair<string, string>("hl", "off"),
                    new KeyValuePair<string, string>("spellcheck", "off")
                };

        }

        internal void AddDefaultBidDateFilter()
        {
            _options.FilterQueries.Add(new SolrQueryByRange<int>("Project.BidDateMonths",
                DateTime.UtcNow.AddYears(-3).MonthsSince20000101(),
                DateTime.UtcNow.AddYears(3).MonthsSince20000101(),
                true));
        }

        internal virtual void AddFilterTypeFilters()
        {
            if (_requestQuery == null)
            {
                _query = new SolrQuery("*:*");
            }
            else
            {
                switch (_requestQuery.FilterType)
                {
                    case TypeOfFilter.Documents:
                        _query = new SolrQuery("*:*");
                        var filters = new List<ISolrQuery>
                        {
                            new SolrQueryByField("ContentType", "Document")
                        };
                        if (!string.IsNullOrEmpty(_requestQuery.Query))
                        {
                            filters.Add(new SolrQuery(_requestQuery.Query));
                        }
                        foreach (var facetName in FacetHelper.DocumentFieldSet)
                        {
                            var facetFilter = _originalRequest.Filters.Find(currentFilter => currentFilter.Name == facetName);
                            if (facetFilter != null)
                            {
                                var filter = GetFacetFilterQuery(facetFilter);
                                if (filter != null)
                                {
                                    filters.Add(filter);
                                }
                            }
                        }
                        if (filters.Count > 1)
                        {
                            var query = new SolrMultipleCriteriaQuery(filters, "AND");
                            _options.FilterQueries.Add(new SolrJoinQuery(query, "Document.ProjectId", "Project.ProjectId"));
                        }
                        break;
                    case TypeOfFilter.Companies:
                        _query = new SolrQueryByField("Project.CompanyNames", _requestQuery.Query);
                        break;
                }
            }
        }

        /// <summary>
        /// Add project facets with filters
        /// </summary>
        internal virtual void AddProjectFacetsWithFacetFilters()
        {
            // counts can be handled with current query facets on single and multi-valued fields
            if (_originalRequest.Aggregate == Aggregation.Count)
            {
                foreach (var facetName in FacetHelper.ProjectFieldSet)
                {
                    var filter = _originalRequest.Filters.Find(currentFilter => currentFilter.Name == facetName);
                    if (filter == null)
                    {
                        ISolrFacetQuery facetQuery = null;
                        switch (facetName)
                        {
                            case "Project.BidDateMonths":
                                facetQuery = new SolrFacetFieldQuery(facetName)
                                {
                                    Sort = false
                                };
                                break;
                            case "Project.Value":
                                facetQuery = GetProjectValueFacetQuery();
                                break;
                            default:
                                if (FacetHelper.FacetSettingCollection.Contains(facetName))
                                {
                                    facetQuery = new SolrFacetFieldQuery(facetName)
                                    {
                                        Limit = -1
                                    };
                                }
                                else
                                {
                                    facetQuery = new SolrFacetFieldQuery(facetName);
                                }
                                break;
                        }
                        _options.AddFacets(facetQuery);
                    }
                    else
                    {
                        AddProjectFacetWithFacetFilter(filter);
                    }
                }
                foreach (var facetName in FacetHelper.MozartOnlyFieldSet)
                {
                    var filter = _originalRequest.Filters.Find(currentFilter => currentFilter.Name == facetName);
                    if (filter != null)
                    {
                        AddProjectFacetFilter(filter);
                    }
                }
            }
            // for all other stats, current query stat facets on single-valued fields can be handled if no filter exists on the field
            // filtered fields (multi-select) need to go through a separate query and multi-valued fields need to go through Aspire
            else
            {
                _options.AddFacets(GetProjectValueFacetQuery());

                var aggregateFieldName = _originalRequest.GetAggregateFieldName();
                // only need count for Project.Value
                var facetFieldNames = FacetHelper.ProjectSingleValuedFieldSet.Where(fieldName => fieldName != "Project.Value").Except(_originalRequest.Filters.Select(filter => filter.Name));
                _options.Stats = new SolrNet.Commands.Parameters.StatsParameters();
                _options.Stats.AddFieldWithFacets(aggregateFieldName, facetFieldNames);

                AddProjectFacetFilters();
            }
        }

        /// <summary>
        /// Add project facet with filter
        /// </summary>
        /// <param name="filter"></param>
        internal virtual void AddProjectFacetWithFacetFilter(Facet filter)
        {
            if (_originalRequest.Aggregate != Aggregation.Count)
            {
                return;
            }
            switch (filter.Name)
            {
                // for filters from Mozart, only apply filter and don't add facet
                case "Company.CountyName":
                case "Company.RoleClassificationParentType":
                case "Project.BidDate":
                case "Project.CountyName_facet":
                case "Project.CreateDate":
                case "Project.ModifyDate":
                case "Project.PostalCode":
                case "Project.StageType_facet":
                    AddProjectFacetFilter(filter);
                    break;
                case "Project.BidDateMonths":
                    // don't use multiselect
                    AddProjectFacetFilter(filter);
                    _options.AddFacets(new SolrFacetFieldQuery(filter.Name));
                    break;
                case "Company.Geography":
                case "Project.CompanyRoleClassificationTypes_facet":
                case "Project.DocumentCSICodesShort":
                case "Project.Geography":
                case "Project.Material_facet":
                case "Project.OwnerType_facet":
                case "Project.SubCategory_facet":
                case "Project.WorkType_facet":
                    if (FacetHelper.FacetSettingCollection.Contains(filter.Name))
                    {
                        _options.AddMultiSelectFacet(filter.Name, filter.FilterValues, null, -1);
                    }
                    else
                    {
                        _options.AddMultiSelectFacet(filter.Name, filter.FilterValues);
                    }
                    break;
                case "Project.Value":
                    AddProjectFacetFilter(filter);
                    _options.AddFacets(GetProjectValueFacetQuery());
                    break;
            }
        }

        internal ISolrFacetQuery GetProjectValueFacetQuery()
        {
            return new SolrFacetRangeQuery("Project.Value", "0", "100000000", "5000000")
            {
                Other = new List<FacetRangeOther>
                {
                    FacetRangeOther.All
                }
            };
        }

        /// <summary>
        /// Add project filters only
        /// </summary>
        internal void AddProjectFacetFilters()
        {
            if (_originalRequest.Filters == null)
            {
                return;
            }
            foreach (var filter in _originalRequest.Filters.Where(filter => FacetHelper.ProjectFieldSet.Contains(filter.Name)))
            {
                AddProjectFacetFilter(filter);
            }
        }

        internal ISolrQuery GetFacetFilterQuery(Facet filter)
        {
            switch (filter.Name)
            {
                // filters from Mozart
                case "Project.BidDate":
                    if (filter.FilterValues.Count == 1)
                    {
                        return new SolrQueryByRange<string>(
                            filter.Name,
                            "NOW/DAY",
                            "NOW/DAY+" + filter.FilterValues.First() + "DAYS");
                    }
                    break;
                case "Project.CreateDate":
                case "Project.ModifyDate":
                    if (filter.FilterValues.Count == 1)
                    {
                        return new SolrQueryByRange<string>(
                            filter.Name,
                            "NOW/DAY-" + filter.FilterValues.First() + "DAYS",
                            "NOW/DAY+1DAYS");
                    }
                    break;
                case "Project.Value":
                    if (filter.FilterValues.Count == 2)
                    {
                        return new SolrQueryByRange<long>(
                            filter.Name,
                            long.Parse(filter.FilterValues[0]),
                            long.Parse(filter.FilterValues[1]));
                    }
                    break;
                case "Project.BidDateMonths":
                    var range = filter.ToBidDateRange();
                    return new SolrQueryByRange<int>(filter.Name, range.Start, range.End);
                case "Company.Geography":
                case "Document.Type":
                case "Project.CompanyRoleClassificationTypes_facet":
                case "Project.DocumentCSICodesShort":
                case "Project.Geography":
                case "Project.Material_facet":
                case "Project.OwnerType_facet":
                case "Project.SubCategory_facet":
                case "Project.WorkType_facet":
                // filters from Mozart
                case "Company.CountyName":
                case "Company.RoleClassificationParentType":
                case "Project.CountyName_facet":
                case "Project.PostalCode":
                case "Project.StageType_facet":
                    return new SolrQueryInList(filter.Name, filter.FilterValues);
            }
            return null;
        }

        /// <summary>
        /// Add project filter
        /// </summary>
        internal virtual void AddProjectFacetFilter(Facet filter)
        {
            var query = GetFacetFilterQuery(filter);
            if (query != null)
            {
                _options.FilterQueries.Add(query);
            }
        }

        public string QueryId
        {
            get
            {
                return QueryName + TypeOfQuery.ToString() + (_queryIdSuffix ?? string.Empty);
            }
        }
        public string QueryName
        {
            get
            {
                if (_requestQuery == null)
                {
                    return string.Empty;
                }
                else
                {
                    return _requestQuery.Name;
                }
            }
        }
        public Uri SolrUri { get; set; }
        public ISolrQuery Query
        {
            get { return _query; }
        }
        public SolrNet.Commands.Parameters.ExtendedQueryOptions Options
        {
            get { return _options; }
        }
        public TypeOfQuery TypeOfQuery { get; set; }
        public SolrResult SolrResult { get; set; }
        public int RequestTimeoutInMilliseconds { get; set; }
    }
}