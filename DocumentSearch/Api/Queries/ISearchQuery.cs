﻿using SolrSearchAPI;

namespace DocumentSearch.Api.Queries
{
    public interface ISearchQuery : IGenericQuery
    {
        string QueryName { get; }
        TypeOfQuery TypeOfQuery { get; }
        SolrResult SolrResult { get; set; }
    }
}