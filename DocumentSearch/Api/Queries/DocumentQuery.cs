﻿using DocumentSearch.Api.DataTransferObjects;
using DocumentSearch.Api.Helpers;
using SolrNet;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DocumentSearch.Api.Queries
{
    public class DocumentQuery : BaseQuery
    {
        List<ISolrQuery> _joinQueryFilters;

        public DocumentQuery(SearchRequestQuery requestQuery, SearchRequest originalRequest, Uri solrUri = null)
            : base(TypeOfQuery.Document, requestQuery, originalRequest)
        {
            SolrUri = solrUri;

            _options.Rows = 25;
            _options.Start = 0;
            _options.ExtraParams = new[]
                {
                    new KeyValuePair<string, string>("hl", "on"),
                    new KeyValuePair<string, string>("spellcheck", "off")
                };

            //_joinQueryFilters = new List<ISolrQuery>
            //{
            //    new SolrQueryByField("ContentType", "Document")
            //};
            //AddFilterTypeFilters();
            //AddProjectFacetFilters();
            //if (_joinQueryFilters.Count > 1)
            //{
            //    var query = new SolrMultipleCriteriaQuery(_joinQueryFilters, "AND");
            //    _options.FilterQueries.Add(new SolrJoinQuery(query, "Project.ProjectId", "Document.ProjectId"));
            //}
            _options.FilterQueries.Add(new SolrQueryByField("ContentType", "Document"));
            _query = new SolrQueryByField("content", requestQuery.Query);

            foreach (var fieldName in FacetHelper.DocumentFieldSet)
            {
                _options.AddFacets(new SolrFacetFieldQuery(fieldName));
            }
        }

        internal override void AddFilterTypeFilters()
        {
            switch (_requestQuery.FilterType)
            {
                case TypeOfFilter.Documents:
                    _query = new SolrQueryByField("content", _requestQuery.Query);
                    break;
                case TypeOfFilter.Companies:
                    var query = new SolrQueryByField("Project.CompanyNames", _requestQuery.Query);
                    _joinQueryFilters.Add(query);
                    break;
            }
        }

        internal override void AddProjectFacetFilter(Facet filter)
        {
            var query = GetFacetFilterQuery(filter);
            if (query == null)
            {
                return;
            }
            _joinQueryFilters.Add(query);
        }
    }
}