﻿using DocumentSearch.Api.Queries;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DocumentSearch.Api
{
    public class ResultCollection : KeyedCollection<string, ISearchQuery>
    {
        public ResultCollection()
        {
        }

        public ResultCollection(IEnumerable<ISearchQuery> queries)
        {
            Add(queries);
        }

        protected override string GetKeyForItem(ISearchQuery item)
        {
            return item.QueryId;
        }

        public void Add(IEnumerable<ISearchQuery> queries)
        {
            foreach (var query in queries)
            {
                Add(query);
            }
        }
    }
}