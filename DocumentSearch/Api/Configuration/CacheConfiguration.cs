﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DocumentSearch.Api.Configuration
{
    public class CacheConfiguration
    {
        [XmlAttribute("enabled")]
        public bool Enabled { get; set; }
        [XmlAttribute("cacheExpirationInMinutes")]
        public int CacheExpirationInMinutes { get; set; }
        [XmlElement("cacheHost", typeof(string))]
        public List<string> CacheHosts { get; set; }
    }
}