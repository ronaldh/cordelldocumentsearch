﻿using System;
using System.Web.Configuration;
using System.Xml.Serialization;


namespace DocumentSearch.Api.Configuration
{
    public class CordellConfiguration
    {
        [XmlElement("projectDetailUri")]
        public string ProjectDetailUriString
        {
            get
            {
                return ProjectDetailUri.ToString();
            }

            set
            {
                ProjectDetailUri = new Uri(value);
            }
        }
        [XmlIgnore()]
        public Uri ProjectDetailUri { get; set; }
        [XmlElement("companyDetailUri")]
        public string CompanyDetailUriString
        {
            get
            {
                return CompanyDetailUri.ToString();
            }

            set
            {
                CompanyDetailUri = new Uri(value);
            }
        }
        [XmlIgnore()]
        public Uri CompanyDetailUri { get; set; }
    }
}
