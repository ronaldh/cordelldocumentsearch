using System;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;

namespace DocumentSearch.Api.Configuration
{
    public class XmlSerializerSectionHandler : IConfigurationSectionHandler
    {
        #region Implementation of IConfigurationSectionHandler

        public object Create(object parent, object configContext, XmlNode section)
        {
            var navigator = section.CreateNavigator();
            var typeName = (string)navigator.Evaluate("string(@type)");

            if (string.IsNullOrEmpty(typeName))
            {
                throw new Exception("\"type\" attribute is missing from config section");
            }

            var xmlRootAttribute = new XmlRootAttribute();
            xmlRootAttribute.ElementName = section.Name;

            var type = Type.GetType(typeName);

            if (type == null)
            {
                throw new Exception("Not able to determine type, " + typeName);
            }

            var serializer = new XmlSerializer(type, xmlRootAttribute);
            return serializer.Deserialize(new XmlNodeReader(section));
        }

        #endregion
    }
}