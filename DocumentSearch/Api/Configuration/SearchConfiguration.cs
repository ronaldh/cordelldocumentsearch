﻿using System;
using System.Xml.Serialization;

namespace DocumentSearch.Api.Configuration
{
    public class SearchConfiguration
    {
        [XmlElement("solrServer")]
        public SolrServer SolrServer { get; set; }
        [XmlElement("aspireAnalyticsServer")]
        public SolrServer AspireAnalyticsServer { get; set; }
        
    }

    public class SolrServer
    {
        [XmlAttribute("uri")]
        public string UriString
        {
            get
            {
                return Uri.ToString();
            }

            set
            {
                Uri = new Uri(value);
            }
        }
        [XmlIgnore()]
        public Uri Uri { get; set; }
    }
}