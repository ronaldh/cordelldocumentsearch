﻿using System.Xml.Serialization;

namespace DocumentSearch.Api.Configuration
{
    public class SessionConfiguration
    {
        [XmlAttribute("sessionExpirationInMinutes")]
        public int SessionExpirationInMinutes { get; set; }
    }
}