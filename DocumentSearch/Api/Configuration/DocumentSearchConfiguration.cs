﻿using System.Web.Configuration;
using System.Xml.Serialization;

namespace DocumentSearch.Api.Configuration
{
    public class DocumentSearchConfiguration
    {
        [XmlElement("cacheConfiguration")]
        public CacheConfiguration CacheConfiguration { get; set; }
        [XmlElement("sessionConfiguration")]
        public SessionConfiguration SessionConfiguration { get; set; }
        [XmlElement("searchConfiguration")]
        public SearchConfiguration SearchConfiguration { get; set; }
        [XmlElement("cordellConfiguration")]
        public CordellConfiguration CordellConfiguration { get; set; }
        
        public static DocumentSearchConfiguration Current()
        {
            return WebConfigurationManager.GetSection("documentSearch") as DocumentSearchConfiguration;
        }
    }
}