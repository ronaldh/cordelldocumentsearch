﻿using DocumentSearch.Api.Configuration;
using DocumentSearch.Api.DataTransferObjects;
using DocumentSearch.Api.Helpers;
using DocumentSearch.Api.Queries;
using SolrSearchAPI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DocumentSearch.Api
{
    internal class SearchManager
    {
        List<ISearchQuery> _queries;
        SolrRequestManager _requestManager;
        SearchRequest _originalRequest;
        SearchConfiguration _searchConfiguration;

        public SearchManager(SearchConfiguration searchConfiguration)
        {
            _searchConfiguration = searchConfiguration;
            _queries = new List<ISearchQuery>();
            _requestManager = new SolrRequestManager(_searchConfiguration.SolrServer.Uri);
        }

        public void HandleRequest(SearchRequest request)
        {
            _originalRequest = request;
            foreach (var query in request.Queries)
            {
                AddQuery(query);
            }
        }

        private void AddQuery(SearchRequestQuery query)
        {
            switch (_originalRequest.ResultType)
            {
                case TypeOfResult.Projects:
                    var projectQuery = new DocumentQuery(query, _originalRequest);
                    _queries.Add(projectQuery);
                    break;
                case TypeOfResult.Companies:
                    break;
                case TypeOfResult.Documents:
                    var docQuery = new DocumentQuery(query, _originalRequest);
                    _queries.Add(docQuery);
                    break;
            }

        }

        public void ExecuteAllQueries()
        {
            var results = _requestManager.ExecuteQueries(_queries.Cast<IGenericQuery>());
            Merge(_queries, results);
            var resultCollection = new DocumentSearch.Api.ResultCollection(_queries);
            Response = new SearchResponse(resultCollection, _originalRequest);
            foreach (var query in resultCollection)
            {
                Response.AddTrace(query);
            }
        }

        private void Merge(IEnumerable<ISearchQuery> queries, IDictionary<string, SolrResult> results)
        {
            foreach (var query in queries)
            {
                if (results.ContainsKey(query.QueryId))
                {
                    query.SolrResult = results[query.QueryId];
                }
            }
        }

        public SearchResponse Response
        {
            get;
            private set;
        }
    }
}